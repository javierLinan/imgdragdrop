// describe('LocalStorageDriver Class', function() {
//   var ls,
//   key,
//   elem1,
//   elem2;

//   beforeEach(function() {
//     ls = new LocalStorageDriver();
//     key = 'key';
//     elem1 = {a:'a',b:'b'};
//     elem2 = {c:'c',d:'d'};
//   });

//   afterEach(function() {
//     localStorage.removeItem(key);
//   });

//   it('should be instantiable', function() {
//     expect(ls instanceof LocalStorageDriver).toBeTruthy();
//   });

//   describe('Method init', function() {
//     it('should set an string like the id for the data keeped in the local storage', function() {
//       ls.init(key);
//       expect(ls.key).toBe(key);
//       expect(typeof ls.key).toBe('string');
//     });

//     it('should load an empty array when the \'key\' is not founded in the local storage', function() {
//       ls.init(key);
//       expect(ls.data).toEqual([]);
//     });

//     it('should load an array of objects when the \'key\' is founded in the local storage', function() {
//       ls.init(key);
//       ls.setData(elem1);
//       expect(ls.data).toEqual([elem1]);
//     });  
//   });

//   describe('Method setData', function() {

//     it('should add to the data array and to the local storage the element passed like argument', function(){
//       var newElem = {e:'e', f:'f'};

//       ls.init(key);
//       ls.setData(newElem);

//       expect(ls.data).toEqual([newElem]);
//       expect(JSON.parse(localStorage.getItem(key))).toEqual([newElem]);
//     });  
//   });
// });