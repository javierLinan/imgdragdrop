function LocalStorageDriver(key) {
  var item = localStorage.getItem(key),
    these = this;

  this.key = key;
  this.data = item ? JSON.parse(item) : [];    

  this.setData = function(elem) {
    these.data.push(elem);
    localStorage.setItem(these.key, JSON.stringify(these.data));
  };

  this.removeData = function(id) {
    var helper = new Helper();

    these.data = helper.getArrWithoutObj(these.data, id);
    localStorage.setItem(these.key, JSON.stringify(these.data));
  };
}