function ImgDriver(depot) {
  var these = this;

  this.depot = depot;

  this.readImg = function(imgObj) {
    var reader = new FileReader();

    reader.onload = function(e) {
      var img = new Image();
      
      img.onload = function() {
        var helper = new Helper(),
          imgData = {
            id: helper.getMaxIdFromArrayObjs(these.depot.data) + 1,
            name: imgObj.name,
            type: imgObj.type,
            size: imgObj.size,
            width: img.width,
            height: img.height,
            src: reader.result
          };

        depot.setData(imgData);
        helper.trigger('imglistwidget.add', { imgData: imgData });
      };
      
      img.src = reader.result; 
    };

    reader.readAsDataURL(imgObj);    
  };

  this.removeImg = function(id) {
    depot.removeData(id);
  };
}