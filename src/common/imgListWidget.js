function ImgListWidget(container) {
  var these = this;

  this.containerId = container;
  this.list = document.querySelector('#' + container + ' .list');
  this.images = document.querySelector('#' + container + ' .images');

  this.init = function(arrImgs) {
    [].forEach.call(arrImgs, function(el) {
      these.addImg(el);
    });
  };

  //Add an element to the list with the name of pictures

  this.createListEl = function(data) {
    var elDom = document.createElement('li'),
      elRemoveDom = document.createElement('a'),
      elInfoDom = document.createElement('span');

    elRemoveDom.addEventListener('click', function(e){
      e.preventDefault();
      e.stopPropagation();
      these.removeImg(data.id);
    }, false);
    elRemoveDom.textContent = 'Remove'; 
    elRemoveDom.classList.add('remove');
    elRemoveDom.classList.add('btn');
    elRemoveDom.classList.add('btn-xs');
    elRemoveDom.classList.add('btn-default');

    elInfoDom.innerHTML = data.width + 'x' + data.height + ' - ' + data.size + ' bytes - <strong>' + data.name + '</strong>';

    elDom.appendChild(elRemoveDom);    
    elDom.appendChild(elInfoDom);    
    elDom.id = 'img-' + data.id; 

    these.list.appendChild(elDom);
  };

  //Add a picture to the list of pictures

  this.createImagesEl = function(data) {
    var elDom = document.createElement('li'),
      img = '<img src="' + data.src + '"/>';

    elDom.id = 'img-' + data.id;
    elDom.innerHTML = img;

    these.images.appendChild(elDom);
  };

  //Add a picture to both lists.

  this.addImg = function(data) {
    these.createListEl(data);
    these.createImagesEl(data);
  };

  this.removeListEl = function(id) {
    var el = document.querySelector('#' + this.containerId + ' .list li#img-' + id);

    if(el) {
      el.parentNode.removeChild(el);
    }
  };

  this.removeImagesEl = function(id) {
    var el = document.querySelector('#' + this.containerId + ' .images li#img-' + id);

    if(el) {
      el.parentNode.removeChild(el);
    }
  };

  this.removeImg = function(id) {
    helper = new Helper();    

    these.removeListEl(id);
    these.removeImagesEl(id);

    helper.trigger('imglistwidget.remove', {removeId: id});
  };

  this.feedingRobot = function() {
    var imgWaiting = document.querySelector('#' + these.containerId + ' .robot img.waiting'),
      imgEating = document.querySelector('#' + these.containerId + ' .robot img.eating');

    imgWaiting.classList.toggle('hidden');
    imgEating.classList.toggle('hidden');
  };

}