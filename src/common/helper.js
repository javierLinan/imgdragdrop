function Helper() {

  function maxIdFromArrayObjs(max, arr) {
    var remainingArr;

    if(arr.length > 0) {
      if(arr[0].id > max) {
        max = arr[0].id;
      }

      remainingArr = arr.slice(1);
      return maxIdFromArrayObjs(max, remainingArr);
    } else {
      return max;
    }
  }

  function removeObjFromArray(newArr, arr, id){
    if(arr.length > 0) {
      if(arr[0].id !== id) {
        newArr.push(arr[0]);
      } 

      remainingArr = arr.slice(1);
      return removeObjFromArray(newArr, remainingArr, id);
    } else {
      return newArr;
    }
  }

  this.trigger = function(eventName, data) {
    var event;

    if (document.createEvent) {
      event = document.createEvent("HTMLEvents");
      event.initEvent(eventName, true, true);
    } else {
      event = document.createEventObject();
      event.eventType = eventName;
    }

    event.eventName = eventName;
    event.data = data;

    if (document.createEvent) {
      document.dispatchEvent(event);
    } else {
      document.fireEvent("on" + event.eventType, event);
    }
  };

  this.getMaxIdFromDomList = function(els) {
    var ids;

    if(!els || els.length === 0) {
      return 0;
    }

    [].forEach.call(els, function(el) {
      ids.push(parseInt(el.id.replace(/^\D+/g, ''), 10));
    });

    return Math.max(ids);
  };

  this.getMaxIdFromArrayObjs = function(arr) {
    return maxIdFromArrayObjs(0, arr);
  };

  this.getArrWithoutObj = function(arr, id) {
    return removeObjFromArray([], arr, id);
  };
}