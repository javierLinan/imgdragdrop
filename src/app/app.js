document.addEventListener('DOMContentLoaded', function () {

  function handleDragover(e) {
    e.stopPropagation();
    e.preventDefault();    
    e.dataTransfer.dropEffect = 'copy';
  }

  function handleDragEnter(e) {
    widget.feedingRobot();
  }    

  function handleDragLeave(e) {
    widget.feedingRobot();
  }  

  function handleDrop(e) {
    e.stopPropagation();
    e.preventDefault();

    imgDriver.readImg(e.dataTransfer.files[0]);
    widget.feedingRobot();
  }

  function handleAddImg(e) {
    e.stopImmediatePropagation();   

    widget.addImg(e.data.imgData);
  }  

  function handleRemoveImg(e) {
    e.stopImmediatePropagation();   

    imgDriver.removeImg(e.data.removeId);
  }
  
  if (Modernizr.draganddrop) {
    var el = document.querySelector('#img-list-container .images'),
    depot = new LocalStorageDriver('key2'),
    widget = new ImgListWidget('img-list-container'),
    imgDriver = new ImgDriver(depot);

    widget.init(imgDriver.depot.data);

    document.addEventListener('imglistwidget.add', handleAddImg, false); 
    document.addEventListener('imglistwidget.remove', handleRemoveImg, false);

    if(el) {
      el.addEventListener('dragover', handleDragover, false);
      el.addEventListener('dragenter', handleDragEnter, false);
      el.addEventListener('dragleave', handleDragLeave, false);
      el.addEventListener('drop', handleDrop, false);
    } 

  } else {
    var alertTarget = document.querySelector('#content-wrapper .alert');
    alertTarget.classList.toggle('hidden');
    alertTarget.textContent = 'It seems that your browser doesn\'t support Drag and Drop Feature';
  }
});

