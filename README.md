## How do I get set up? ##

Install Node.js and then:


```
#!javascript

$ git clone https://javierLinan@bitbucket.org/javierLinan/imgdragdrop.git
$ cd imgdragdrop
$ sudo npm -g install grunt-cli karma bower
$ npm install
$ bower install
$ grunt build
$ The index.html file will be in the build folder

```